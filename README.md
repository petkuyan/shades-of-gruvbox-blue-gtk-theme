# Shades-of-gruvbox
theme based on Shades-of-gray theme by WernerFP.

Color scheme from https://github.com/morhetz/gruvbox
Theme based on https://github.com/WernerFP/Shades-of-gray-theme


## How to use
1. Put in ~/.themes/
2. Use tweaks tool to select Shades-of-gruvbox-blue for the applications theme 

For more selected colors - activate in gtk-colors.css from line 12 to 37.

## Support
If you want support me - paypal.me/petkuyan